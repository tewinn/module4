import sys
import re
import os.path

# Begin method declarations

# Prints out the usage of the script and then exits
# Input: None
#Output: None
def PrintUsage():
	print("Usage:\n[Username]$ python3 baseball.py \"BoxScoreFile.txt\"\n.txt file must be in the same directory.\nExiting Now")
	sys.exit()

# Reads a file line by line and matches regular expressions to find a baseball season's data 
# Input: scoreFileName: file to read in
# Output:  A tuple that contains 2 dictionaries in the form of ["Player's Name", number of times at bat] and ["Player's Name", number of hits] respectively
def ReadSeasonStats(scoreFileName):
	# Initialize return dictionaries
	batDict={}
	hitDict={}
	
	# Defines regular expression patterns
	playerPat="^\\b[A-Z][a-z]*\\b \\b[A-Z][a-z]*\\b"
	battedPat="batted [0-9]+"
	hitsPat="[0-9]+ hits"
	numberPat="[0-9]+"
	
	# Check to see if file exists
	if(os.path.isfile(scoreFileName)==False):
		PrintUsage()

	# Open file
	scoreFile=open(scoreFileName, 'r')
	
	# Read each line in file and regex for data
	for scoreLine in scoreFile:
		# Search using regular expressions
		playerSearch=re.search(playerPat, scoreLine)
		battedSearch=re.search(battedPat, scoreLine)
		hitsSearch=re.search(hitsPat, scoreLine)
		if(playerSearch!=None and battedSearch!=None and hitsSearch!=None):
			# Get player's name
			playerName=playerSearch.group()
			
			# Get number of bats
			batNumberSearch=re.search(numberPat, battedSearch.group())
			batted=batNumberSearch.group()
			
			# Get number of hits
			hitNumberSearch=re.search(numberPat, hitsSearch.group())
			hits=hitNumberSearch.group()
			
			# Add values to dictionaries
			if(playerName in batDict.keys()):
				batDict[playerName]=batDict[playerName]+float(batted)
				hitDict[playerName]=hitDict[playerName]+float(hits)
			else:
				batDict[playerName]=float(batted)
				hitDict[playerName]=float(hits)

	# Close file
	scoreFile.close()
	
	# Return dictionaries
	return batDict, hitDict

# Computes batting average
# Input: batDict: a dictionary in the form of ["Player's Name", number of times at bat]; hitDict: a dictionary in the form of ["Player's Name", number of hits]
# Output: A dictionary with the form of ["Player's Name", batting average]
def ComputeBattingAverage(batDict, hitDict):
	# Initialize return dictionary
	avgDict={}
	# Compute average
	for [playerName, bat] in batDict.items():
		avg=hitDict[playerName]/bat
		avgDict[playerName]=avg
		
	# Return dictionary
	return avgDict

# Begin main method

# Checks number of arguments passed to script
if (len(sys.argv)==1):
	PrintUsage()

# Set file name 
scorefile=sys.argv[1]

# Read in file
batDict, hitDict=ReadSeasonStats(scorefile)

# Compute averages
avgDict=ComputeBattingAverage(batDict, hitDict)

# Sort
sortedAvg= sorted((value, key) for (key,value) in avgDict.items())

# Print out "Player's Name: batting average" for each player
for (battingAverage, playerName) in reversed(sortedAvg):
	print("%s: %.3f" % (playerName, round(battingAverage, 3)))

# Exit
sys.exit()
